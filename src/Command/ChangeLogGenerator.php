<?php

namespace SyncHot\ChangeLoggen\Command;

use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use SyncHot\ChangeLoggen\Exceptions\LastTagException;

class ChangeLogGenerator extends Command
{
    private $changeLogFile = '/../../../../CHANGELOG.md';
    private $changeLogFileLocal = 'CHANGELOG.md';
    private $changeLogData;
    protected static $defaultName = 'log:gen';
    private $lastTag;
    private $prevLastTag;
    private $releaseTag;
    private $commitTypes = [
        'feat' => 'New features',
        'fix' => "Bug fixes",
        'build' => "New features",
        'chore' => 'Documentation',
        'ci' => '',
        'docs' => 'Documentation',
        'style' => 'Bug fixes',
        'refactor' => 'Bug fixes',
        'perf' => 'Performance',
        'test' => 'Bug fixes',
    ];
    private const FORMAT_DATE = 'Y.m.d';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure()
    {
        $this
            ->setDescription('Changelog generator');
    }

    /**
     * @param InputInterface $input 
     * @param OutputInterface $output 
     * @return int 
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try{
            $this->setLastTags();
        }catch(LastTagException $ex){
            $io->error($ex->getMessage());
            return 1;
        }
        $this->changeLogData = $this->parseCommits($this->getCommitSinceTag($this->lastTag));
        $this->setReleaseTag();
        $formatedChangeLogData = $this->formatChangeLogData($this->changeLogData);

        $this->writeChangeLog($formatedChangeLogData);

        $pushCommit = $io->confirm("Do You want to push release with tag: " . $this->releaseTag, true);

        if ($pushCommit) {
            $commit['add']  = `git add .`;
            $commit['msg']  = `git commit -m "docs: Changelog generated for $this->releaseTag"`;
            $commit['tag']  = `git tag v$this->releaseTag`;
            $commit['push'] = `git push origin --tags`;
            $commit['push'] = `git push origin`;
        }

        $io->success('Changelog generated successfully and pushed to repository.');
        return 0;
    }

    /**
     * @return void 
     */
    private function setLastTags()
    {
        $gitResult  = `git tag -l --sort=-v:refname`;
        $gitResults = explode("\n", $gitResult);
        if(empty($gitResult)){
            throw new LastTagException("Please tag commit: v1.0.0");
        }
        $this->lastTag = str_replace('v', '', $gitResults[0]);
        if (!empty($gitResults[1])) {
            $this->prevLastTag = str_replace('v', '', $gitResults[1]);
        }
    }

    /**
     * @param mixed $tag 
     * @return array 
     */
    private function getCommitSinceTag($tag): array
    {
        $outcome = [];
        $output = `git log v$tag..HEAD --oneline --no-merges --pretty="%s----DELIMITER----%h"`;
        $output = explode("\n", $output);

        foreach ($output as $result) {
            if (!empty($result)) {
                $outcome[] = $result;
            }
        }
        return $outcome;
    }

    private function getCommitSinceTagMultiLiner($tag): array
    {
        $outcome = [];
        $output = `git log v$tag..HEAD --no-merges`;
        $output = explode("\n", $output);

        foreach ($output as $result) {
            if (!empty($result)) {
                $outcome[] = $result;
            }
        }

        return $outcome;
    }

    /**
     * @param mixed $commits 
     * @return array 
     */
    private function parseCommits($commits): array
    {
        $output = [];
        foreach ($commits as $commit) {
            $commitMsgStruct = explode("----DELIMITER----", $commit);
            $message = explode(":", $commitMsgStruct[0]);
            if (!empty($message[1])) {
                $type = $message[0];
                $description = $message[1];
                $hash = $commitMsgStruct[1];

                foreach (array_keys($this->commitTypes) as $commitType) {
                    if (preg_match('/' . $commitType . '/i', $type)) {
                        $output[$commitType][] = trim("$description ($hash)");
                    }
                }
            }
        }

        return $output;
    }

    /**
     * @param mixed $changeLogData 
     * @return string 
     */
    private function formatChangeLogData($changeLogData): string
    {

        $output = str_repeat('#', $this->releaseType + 1) . " $this->releaseTag " . '(' . $this->getDate() . ")\n\n\n";

        foreach (array_keys($this->commitTypes) as $commitType) {

            if (isset($changeLogData[$commitType])) {
                $output .= "## " . $this->commitTypes[$commitType] . "\n";
                foreach ($changeLogData[$commitType] as $structEl) {
                    $output .= "* $structEl \n";
                }
            }
        }

        return $output;
    }

    /**
     * 
     * @param mixed $logData 
     * @return void 
     */
    private function writeChangeLog($logData): bool
    {
        if (!$this->logDataExists($logData) || !file_exists($this->getChangeLogFile())) {
            $fileContents = $this->readFile();
            file_put_contents($this->getChangeLogFile(), $logData . "\n" . $fileContents);
            return true;
        }
        return false;
    }

    /**
     * 
     * @param mixed $logData 
     * @return bool 
     */
    private function logDataExists($logData): bool
    {
        if (strpos($this->readFile(), $logData) > 0) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @return string 
     */
    private function readFile(): string
    {
        try {
            if (!file_exists($this->getChangeLogFile())) {
                file_put_contents($this->getChangeLogFile(), "");
            }
            return  file_get_contents($this->getChangeLogFile());
        } catch (\Exception $ex) {
            return "";
        }
    }

    /**
     * @return string 
     */
    private function  getChangeLogFile(): string
    {
        if (!file_exists($this->changeLogFile)) {
            return $this->changeLogFileLocal;
        }
        return $this->changeLogFile;
    }

    /**
     * @return void 
     */
    private function setReleaseTag(): void
    {
        $lastTag = explode(".", $this->lastTag);
        
        $version['major'] = $lastTag[0];
        $version['minor'] = $lastTag[1];
        $version['patch'] = $lastTag[2];
        $multiLinerChangeLog = $this->getCommitSinceTagMultiLiner($this->lastTag);
        
        foreach ($multiLinerChangeLog as $str) {
            if (preg_match("/BREAKING CHANGE/i", $str)) {
                $this->releaseTag = ($version['major'] + 1) . '.0.0';
                $this->releaseType = 0;
                return;
            }
        }

        if (!empty($this->changeLogData['feat'])) {
            $this->releaseTag = $version['major'] . '.' . ($version['minor'] + 1) . '.0';
            $this->releaseType = 1;
            return;
        }

        $this->releaseTag = $version['major'] . '.' . $version['minor'] . '.' . ($version['patch'] + 1);
        $this->releaseType = 2;
    }

    private function getDate(){
        return (new DateTime())->format(self::FORMAT_DATE);
    }
}
