# CHANGELOGGEN
> Changelog generator based on conventional commits.

### Installation
```
composer require --dev synchot/changeloggen
```

### Usage:
```
./vendor/bin/changeloggen log:gen
```
### It is recommended to use with 
```
composer require --dev synchot/conventionalcommit
```
### CHANGELOG
[https://bitbucket.org/SyncHot/changeloggen/src/master/CHANGELOG.md](https://bitbucket.org/SyncHot/changeloggen/src/master/CHANGELOG.md)

### Todos
- Refactor
- Write Tests

License
----

MIT

