# 4.0.0 (2019.12.17)


## New features
* changing conventionalcommit version (1b843d1) 

### 3.0.2 (2019.12.15)


## Documentation
* date formated (ff9db55) 

### 3.0.1 2019.12.15


## Bug fixes
* added date to changelog (fb6274f) 

# 3.0.0
## Bug fixes
* fixed breaking change generator (e040043) 

### 2.2.2 (2019-12-14)


## Bug fixes

* code style fixes (deffcaf) 

### 2.2.1 (2019-12-14)


## Bug fixes

* vendor version downgraded (50ef305) 

## 2.2.0 (2019-12-14)


## New features

* somme (2056498) 
## Bug fixes

* something (e26688d) 
## Bug fixes

* Test commit (3ba0a57) 

### 2.1.1 (2019-12-14)


## Bug fixes

* new line after date removed (8946d7d) 

## 2.1.0 (2019-12-14)


## New features

* added date generated and formatting fixes (cdfd021) 

### 2.0.10
## Documentation
* Writing better readme.md (d9bf260) 

### 2.0.9
## Documentation
* removed wrong readme name (f73dd11) 

### 2.0.8
## Documentation
* readme fixes (b1a28e2) 

### 2.0.7
## Bug fixes
* git push not only tags (7235796) 

### 2.0.6
## Documentation
* composer.json changes (6120e91) 

### 2.0.5
## Documentation
* another readme fixes - will never end (7b2c19f) 

### 2.0.4
## Documentation
* readme fix (732c434) 
* message about push when changelog generated (84429b7) 

### 2.0.3
## Bug fixes
* symfonystylefixes (84998da) 
## Documentation
* readme update (84da00b) 

### 2.0.2
## Documentation
* readme update (6c77479) 

### 2.0.1
## Bug fixes
* missing catch exception for not existing tag (0816836) 
## Documentation
* readme update (5f74d29) 
* Modified changelog (73a61b6) 

# 2.0.0
## New features
* Changelog gen fixes (87f82e3) 
## Bug fixes
* Not existing changelog.md fix (05cec8e) 
* Fix for CHANGELOG topics (bcbf48e) 
* Added function description (96f215d) 
## Documentation
* Adding composer convcommit (801849a) 
* BREAKING CHANGE (fa5cae8) 
## Documentation
* Changelog generated for 2.1.0 (98b6ab7) 
* Changelog edited manually for backward com (b2908da) 
* Changelog generated v2.0.2 (f666282) 
* Changelog generated v2.0.1 (73a77f2) 
* Changelog generated v2.0.0 (0f9002e) 
* Checking if tags work fine (06b029f) 
* Changelog generated v2.0.0 (16db9c9) 
* Changelog generated v2.0.0 (abcf5f8) 
* Changelog generated v2.0.0 (8e59890) 
* Changelog generated v2.0.0 (f990867) 
* Changelog generated v2.0.0 (d91fcb6) 
* Changelog generated v2.0.0 (49ac425) 
* Changelog generated v2.0.0 (3a16ed8) 
* Changelog generated v1.1.0 (cd47992) 
* Changelog generated v1.1.0 (4cad9f5) 
* Changelog generated v1.1.0 (d20bcfa)